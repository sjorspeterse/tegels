SERIAL_PORT = '/dev/ttyUSB0'
BAUD_RATE = 1000000

import serial
from time import sleep

sp = serial.Serial(SERIAL_PORT, BAUD_RATE, timeout = 1)
sp.flush()
print ('Serial name:', sp.name)

while(1):
    # response = sp.readline()
    response = sp.read(1)
    if response:
        # print(response.decode().strip(), flush=True)
        print(response)
    else:
        print('Restarting!')
        sleep(1)
    
    sp.write([0])
