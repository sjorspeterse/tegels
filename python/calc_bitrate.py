SERIAL_PORT = '/dev/ttyUSB0'
# BAUD_RATE = 9600
# BAUD_RATE = 14400
BAUD_RATE = 1000000

import serial
from time import sleep, time

sp = serial.Serial(SERIAL_PORT, BAUD_RATE, timeout = 1)
sp.flush()
print ('Serial name:', sp.name)

DATA = [0] * 61 * 3

starttime = time()
while(1):
    sp.write(DATA)
    if time() > starttime + 1:
        sp.write([1])
        starttime = time()
        response = sp.readline()
        if response:
            print(response.decode().strip(), flush=True)
