from math import sqrt

def print_results(led_distance: float, width: float):
    print('LED distance =  %.2f mm' % led_distance)
    print('Tegel width = %.2f mm' % width)
    print('Tegel height = %.2f mm' % (0.5 * sqrt(3) * width))

def calculate_single_row(d: float, D_min: float):
    D = 2/3*sqrt(3)*d
    if D < D_min:
        print('setting D to minimum clearance value')
        D = D_min
    width = 10 * D
    return D, width

def calculate_triple_row(D_single: float, D_min: float):
    D = D_single / 2
    if D < D_min:
        print('setting D to minimum clearance value')
        D = D_min
    width = 12 * D
    return D, width

def calculate_both(led_size: float, led_gap: float, clearance: float):
    print('Settings: LED size: %.1f, led gap: %.1f, edge clearance: %0.1f' % 
        (led_size, led_gap, clearance))
    d = clearance + 0.25 * led_size * (1 + sqrt(3))
    D_min = 2/3*sqrt(3)*(led_gap + led_size)
    print('D_min: %.2f' % D_min)
    
    print('\nWith an edge with of 1 led:')
    D1, W1 = calculate_single_row(d, D_min)
    print_results(D1, W1)

    print('\nWith an edge with of 3 leds:')
    D3, W3 = calculate_triple_row(D1, D_min)
    print_results(D3, W3)
    print()

if __name__ == '__main__':
    calculate_both(led_size=2, led_gap=1, clearance=3.7)