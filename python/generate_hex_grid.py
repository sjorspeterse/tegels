import matplotlib.pyplot as plt
import math
from collections import namedtuple 


def sin(degrees):
    return math.sin(degrees * 2 * math.pi /360)

def cos(degrees):
    return math.cos(degrees * 2 * math.pi /360)

Point = namedtuple('Point', ['x', 'y'], defaults=[0, 0])
def plot_outline():
    TOP_LEFT = Point(0.25, 1)
    TOP_RIGHT = Point(1 - TOP_LEFT.x, 1)
    LEFT = Point(0, 0.5)
    RIGHT = Point(1, 0.5)
    BOTTOM_LEFT = Point(TOP_LEFT.x, 0)
    BOTTOM_RIGHT = Point(TOP_RIGHT.x, 0)
    plt.plot(
        [TOP_LEFT.x, TOP_RIGHT.x, RIGHT.x, BOTTOM_RIGHT.x, BOTTOM_LEFT.x,
        LEFT.x, TOP_LEFT.x], 
        [TOP_LEFT.y, TOP_RIGHT.y, RIGHT.y, BOTTOM_RIGHT.y, BOTTOM_LEFT.y,
        LEFT.y, TOP_LEFT.y]
        )

def plot_points(leds_per_side):
    x_list = []
    y_list = []
    TOP_LEFT = Point(0.25, 1)
    cur_point = TOP_LEFT
    inter_distance = 0.5/(leds_per_side - 1)
    increment = inter_distance
    for row in range(leds_per_side-1):
        for _ in range(leds_per_side + row):
            x_list.append(cur_point.x)
            y_list.append(cur_point.y)
            cur_point = Point(cur_point.x + increment, cur_point.y)
        increment = -increment
        cur_point = Point(cur_point.x + 0.5*increment, cur_point.y - inter_distance)
    for row in range(leds_per_side):
        for _ in range(2*leds_per_side - 1 - row):
            x_list.append(cur_point.x)
            y_list.append(cur_point.y)
            cur_point = Point(cur_point.x + increment, cur_point.y)
        increment = -increment
        cur_point = Point(cur_point.x + 1.5*increment, cur_point.y - inter_distance)

    x_int_list = [int(x * 255) for x in x_list]
    y_int_list = [int(y * 255) for y in y_list]
    # for x, y in zip(x_int_list, y_int_list):
        # print(x, y)
    print('x list:', x_int_list)
    print('y list:', y_int_list)
    plt.scatter(x_list, y_list)

def plot_figure(leds_per_side):
    PLOT_SCALE_FACTOR = 3
    PLOT_WIDTH = 2 * PLOT_SCALE_FACTOR
    PLOT_HEIGHT = 2 * sin(60) * PLOT_SCALE_FACTOR
    plt.figure(figsize=(PLOT_WIDTH, PLOT_HEIGHT))
    plot_outline()
    plot_points(leds_per_side)
    plt.show()

if __name__ == '__main__':
    LEDS_PER_SIDE=5
    plot_figure(LEDS_PER_SIDE)