import select
import sys
import tkinter as tk
import pandas as pd
import signal
import math
import time
from collections import namedtuple 

Point = namedtuple('Point', ['x', 'y'], defaults=[0, 0])

def calc_cirlces(leds_per_side, total_width, total_height):
    x_list = []
    y_list = []
    TOP_LEFT = Point(0.25, 0)
    cur_point = TOP_LEFT
    inter_distance = 0.5/(leds_per_side - 1)
    increment = inter_distance
    for row in range(leds_per_side-1):
        for _ in range(leds_per_side + row):
            x_list.append(cur_point.x)
            y_list.append(cur_point.y)
            cur_point = Point(cur_point.x + increment, cur_point.y)
        increment = -increment
        cur_point = Point(cur_point.x + 0.5*increment, cur_point.y + inter_distance)
    for row in range(leds_per_side):
        for _ in range(2*leds_per_side - 1 - row):
            x_list.append(cur_point.x)
            y_list.append(cur_point.y)
            cur_point = Point(cur_point.x + increment, cur_point.y)
        increment = -increment
        cur_point = Point(cur_point.x + 1.5*increment, cur_point.y + inter_distance)
    x_list = [i*total_width for i in x_list]
    y_list = [i*total_height for i in y_list]
    return x_list, y_list

def create_circle(canvas, x, y, r):
    x0 = x - r
    y0 = y - r
    x1 = x + r
    y1 = y + r
    return canvas.create_oval(x0, y0, x1, y1)

def handler(signum, frame):
    print('Exiting')
    sys.exit(1)

meantime = 1
jsontime = 1
configtime = 1
updatetime = 1
def new_frame(line, canvas, leds):
    t1 = time.time()
    # df = pd.read_json(line)
    # df = pd.read_json(line, orient='split')
    print(line)
    df = pd.DataFrame(eval(line))
    t11 = time.time()
    rgb = list(zip(df.r, df.g, df.b))
    for i, color in enumerate(rgb):
        colorstring = '#%02x%02x%02x' % color
        canvas.itemconfig(leds[i], fill=(colorstring))

    t2 = time.time()
    canvas.update()
    t3 = time.time()
    global meantime, jsontime, configtime, updatetime
    meantime = 0.99*meantime + 0.01*(t3-t1)
    jsontime = 0.99*jsontime + 0.01*(t11-t1)
    configtime = 0.99*configtime + 0.01*(t2-t11)
    updatetime = 0.99*updatetime + 0.01*(t3-t2)

    print('mean time:', meantime, 'json:', jsontime, 'config:', configtime, 'update:', updatetime, end='\n\r')


def read_input(canvas, leds):
    while sys.stdin in select.select([sys.stdin], [], [], 0)[0]:
        line = sys.stdin.readline()
        if line:
            new_frame(line, canvas, leds)
        else: 
            print('eof')
            exit(0)

def check_input(canvas, leds):
    read_input(canvas, leds)
    canvas.after(1, lambda: check_input(canvas, leds))

def create_leds(canvas, xlist, ylist, radius):
    leds = [0] * len(xlist)
    for i, (x, y) in enumerate(zip(xlist, ylist)):
        leds[i] = create_circle(canvas, x+1.5*radius, y+1.5*radius, radius)
    return leds

def main():
    RADIUS = 30
    MARGIN = 3*RADIUS
    WIDTH = 1000
    HEIGHT = int(WIDTH * math.sin(math.pi / 3)) # 60 degrees
    signal.signal(signal.SIGINT, handler)


    win = tk.Tk()
    win.geometry(str(WIDTH+MARGIN) + 'x' + str(HEIGHT+MARGIN))
    win.update()
    win.title('Nutteloze zeshoek')

    canvas = tk.Canvas(win,width=(WIDTH+MARGIN), height=(HEIGHT+MARGIN))
    canvas.configure(bg='black')
    canvas.pack()

    xlist, ylist = calc_cirlces(5, WIDTH, HEIGHT)
    leds = create_leds(canvas, xlist, ylist, RADIUS)

    canvas.after(0, lambda: check_input(canvas, leds))
    win.mainloop()


if __name__ == '__main__':
    main()
