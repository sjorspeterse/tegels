import pandas as pd
import numpy as np
from datetime import datetime
from matplotlib.animation import FuncAnimation
import time

import matplotlib.pyplot as plt
import math
from collections import namedtuple 

def sin(degrees):
    return math.sin(degrees * 2 * math.pi /360)

def cos(degrees):
    return math.cos(degrees * 2 * math.pi /360)

PLOT_SCALE_FACTOR = 5
PLOT_WIDTH = 2 * PLOT_SCALE_FACTOR
PLOT_HEIGHT = 2 * sin(60) * PLOT_SCALE_FACTOR

x_data, y_data = [], []

figure = plt.figure(figsize=(PLOT_WIDTH, PLOT_HEIGHT))
ax = plt.axes()
ax.set_facecolor('black')
x_int_list = []
y_int_list = []

Point = namedtuple('Point', ['x', 'y'], defaults=[0, 0])

def plot_points(leds_per_side):
    x_list = []
    y_list = []
    TOP_LEFT = Point(0.25, 1)
    cur_point = TOP_LEFT
    inter_distance = 0.5/(leds_per_side - 1)
    increment = inter_distance
    for row in range(leds_per_side-1):
        for _ in range(leds_per_side + row):
            x_list.append(cur_point.x)
            y_list.append(cur_point.y)
            cur_point = Point(cur_point.x + increment, cur_point.y)
        increment = -increment
        cur_point = Point(cur_point.x + 0.5*increment, cur_point.y - inter_distance)
    for row in range(leds_per_side):
        for _ in range(2*leds_per_side - 1 - row):
            x_list.append(cur_point.x)
            y_list.append(cur_point.y)
            cur_point = Point(cur_point.x + increment, cur_point.y)
        increment = -increment
        cur_point = Point(cur_point.x + 1.5*increment, cur_point.y - inter_distance)

    global x_int_list
    x_int_list = [int(x * 255) for x in x_list]
    global y_int_list
    y_int_list = [int(y * 255) for y in y_list]

def plot_outline():
    TOP_LEFT = Point(0.25, 1)
    TOP_RIGHT = Point(1 - TOP_LEFT.x, 1)
    LEFT = Point(0, 0.5)
    RIGHT = Point(1, 0.5)
    BOTTOM_LEFT = Point(TOP_LEFT.x, 0)
    BOTTOM_RIGHT = Point(TOP_RIGHT.x, 0)
    plt.plot(
        [TOP_LEFT.x, TOP_RIGHT.x, RIGHT.x, BOTTOM_RIGHT.x, BOTTOM_LEFT.x,
        LEFT.x, TOP_LEFT.x], 
        [TOP_LEFT.y, TOP_RIGHT.y, RIGHT.y, BOTTOM_RIGHT.y, BOTTOM_LEFT.y,
        LEFT.y, TOP_LEFT.y]
        )

plot_points(5)
plot_outline()
scatter = plt.scatter(x_int_list, y_int_list, s=500)
plt.subplots_adjust(top=1.0, bottom=0.0, left=0.0, right=1.0, hspace=0.2, wspace=0.2)
def update(frame):
    t1 = time.time()
    data = input()
    df = pd.read_json(data)
    colors = [(r/255, g/255, b/255) for (r, g, b) in list(zip(df.r, df.g, df.b))]
    scatter.set_color(colors)
    print('took', (time.time() - t1), 'seconds')
    return scatter,

def plot_figure(leds_per_side):
    plot_outline()
    plot_points(leds_per_side)

def main():
   animation = FuncAnimation(figure, update, interval=1000/60)
   plt.show()

if __name__ == '__main__':
   main()


