import cv2 as cv
import numpy as np

LEDS_PER_ROW = [5, 6, 7, 8, 9, 8, 7, 6, 5]
TOTAL_COLORS = sum(LEDS_PER_ROW)*3
HEIGHT = len(LEDS_PER_ROW)
WIDTH = max(LEDS_PER_ROW) * 2 - 1
FILE_NAME = 'FRAME_DATA.py'

def print_stats(video_capture):
    length = int(video_capture.get(cv.CAP_PROP_FRAME_COUNT))
    fps = video_capture.get(cv.CAP_PROP_FPS)

    print('length = ', length)
    print('framerate = ', fps)

def read_frame(frame):
    ledframe = np.zeros((61 * 3), dtype=int)
    col = LEDS_PER_ROW[0] - 1
    increment = 2
    ledID = 0
    # Sample the LEDS in a snake-like fashion
    for row, leds in enumerate(LEDS_PER_ROW):
        for _ in range(leds):
            blue = frame[row][col][0]
            green = frame[row][col][1]
            red = frame[row][col][2]
            ledframe[ledID] = red
            ledframe[ledID+1] = green
            ledframe[ledID+2] = blue
            ledID += 3
            col += increment
        increment = -increment
        col += increment//2
        if row >= 4: # Bottom half
            col += increment
    return ledframe

def create_file(size):
    with open(FILE_NAME, 'w') as f:
        f.write('frames = [')

def write_to_file(frame_data, last=False):
    with open(FILE_NAME, 'a') as f:
        f.write('[')
        for i in range(TOTAL_COLORS-1):
            f.write('%i, ' % frame_data[i])
        endwith = ']' if last else '],\n'
        f.write('%i' % frame_data[TOTAL_COLORS-1] + endwith)

def close_file():
    with open(FILE_NAME, 'a') as f:
        f.write(']\n')

def main():
    cap = cv.VideoCapture('../videos/hexagon_anim1.mov')
    print_stats(cap)

    nr_frames = int(cap.get(cv.CAP_PROP_FRAME_COUNT))
    create_file('%i*61*3' % nr_frames)

    for i in range(nr_frames-1):
        ret, frame = cap.read()
        ledframe = read_frame(frame)
        write_to_file(ledframe)

    ret, frame = cap.read()
    ledframe = read_frame(frame)
    write_to_file(ledframe, last=True)

    cap.release()

    close_file()

if __name__ == '__main__':
    main()