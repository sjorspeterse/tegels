import serial
import pandas as pd
import numpy as np
from time import sleep

FILE_NAME = 'framebuffer.csv'
from data.MANDELBROT import frames as video1
from data.KNIKKERS import frames as video2
from data.FLOW import frames as video3
from data.PULSE import frames as video4

def flash_green():
    from math import sin
    t = 0
    while(1):
        data = [0, int(100*(sin(t) + 1)), 0] * 61
        dataframe = pd.DataFrame({'r': [0]*61, 'g':[int(100*(sin(t)+1))]*61, 'b':[0]*61})
        dataframe.to_csv(FILE_NAME, index_label='index')
        result = dataframe.to_json(None)
        sleep(1/60)
        t += 0.1

def play_video():
    while(1):
        for video in [video1, video2, video3, video4]:
            for frame in video:
                [r, g, b] = np.reshape(frame, (3, 61), order='F')
                dataframe = pd.DataFrame({'r': r, 'g': g, 'b':b})
                dataframe.to_csv(FILE_NAME, index_label='index')
                result = dataframe.to_json(None)
                sleep(1/60)

if __name__ == '__main__':
    # flash_green()
    play_video()
