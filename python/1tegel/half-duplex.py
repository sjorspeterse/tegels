SERIAL_PORT = '/dev/ttyUSB0'
BAUD_RATE = 1000000
START_MESSAGE = 255
SLEEP_TIME = 0.0020 # ms
MASTER_ADDR  = 0

import serial
from time import sleep, time

counts = [0] * 15
buffer = bytes()
DATA = [ord('a')] * 61 * 3

def handle_message(message, source, destination):
    # print('from %i to %i, len=%i' % (source, destination, len(message)), message)
    if source == MASTER_ADDR:
        return
    
    if destination != MASTER_ADDR:
        print('Warning! Message not directed to master')
        return
    
    check = int(message)
    if check != 183:
        print('Warning! Message', source, 'has length ', check, 'instead of 183') 
    if source >= 16:
        print('Warning! Index >= 16')
        return
    counts[source-1] += 1

def handle_chunk(chunk):
    global buffer
    chunk = buffer + chunk
    i = 0
    while i < len(chunk)-3:
        d = chunk[i]
        if d != START_MESSAGE:
            i += 1
            continue

        source_addr = chunk[i+1]
        dest_addr = chunk[i+2]
        if source_addr > 15 or dest_addr > 15 or not(bool(source_addr) ^ bool(dest_addr)):
            i += 1
            continue

        length = chunk[i+3]
        if i+4+length >= len(chunk):
            break

        message = chunk[i+4: i+4+length]
        handle_message(message, source_addr, dest_addr)
        i += 4 + length

    buffer = chunk[i:]

sp = serial.Serial(SERIAL_PORT, BAUD_RATE, timeout = 0)
sp.flush()
sp.flushInput()
sp.flushOutput()
print ('Serial name:', sp.name)

while(True):
    starttime = time()
    for slave_addr in range(1, 16):
        sp.write([START_MESSAGE, MASTER_ADDR, slave_addr, len(DATA)] + DATA)
        sleep(SLEEP_TIME)
    chunk = sp.read(4000)
    handle_chunk(chunk)
    fps = 1/(time() - starttime) 
    diff = (max(counts) - min(counts))

    print('counts:', counts, 'diff:', diff, 'fps: %.2f' % fps)
    for i in range(14):
        assert counts[i] >= counts[i+1]
