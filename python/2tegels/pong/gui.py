import sys
import signal
import tkinter as tk
import pandas as pd
import numpy as np


class GUI:

    def __init__(self, board):
        self.win = tk.Tk()
        self.board = board
        self.win.geometry('1000x800')
        self.win.title('Nutteloze zeshoek')

        self.canvas = tk.Canvas(self.win)
        self.canvas.configure(bg='#707070')

        signal.signal(signal.SIGINT, self.interrupt_handler)
        self.canvas.pack(fill=tk.BOTH, expand=True)

    def create_circle(self, canvas, x, y, r):
        '''Create a single circle/LED and add to the canvas.'''
        x0 = x - r
        y0 = y - r
        x1 = x + r
        y1 = y + r
        return canvas.create_oval(x0, y0, x1, y1)

    def interrupt_handler(self, signum, frame):
        print('Exiting')
        sys.exit(1)

    def calc_xy(self, df):
        from board import Board
        '''Add x and y coordinates to the dataframe.'''
        ab = np.array([df.a, df.b]).T
        xy = Board.ab2xy(ab)
        df['x'] = xy[:, 0]
        df['y'] = xy[:, 1]
        return df

    def get_dataframe(self):
        df = pd.DataFrame()
        for hexagon in self.board.hexagons:
            df = df.append(hexagon.get_dataframe(), ignore_index=True)

        df = self.calc_xy(df)
        return df

    def update_transformation(self, df):
        x, y = df.x, df.y
        self.shift1x = (min(x) + max(x)) / 2
        self.shift1y = (min(y) + max(y)) / 2
        width, height = self.canvas.winfo_width(), self.canvas.winfo_height()
        xdiff, ydiff = max(x) - min(x), max(y) - min(y)
        xfactor, yfactor = width / xdiff, height / ydiff
        self.factor = min(xfactor, yfactor) * 0.8  # Add margin
        self.shift2x, self.shift2y = width/2, height/2

    def transform_xy(self, x, y):
        x = (x - self.shift1x) * self.factor + self.shift2x
        y = -(y - self.shift1y) * self.factor + self.shift2y
        return x, y

    def add_leds(self, df):
        '''Show a new frame on the screen.'''
        x, y = self.transform_xy(df.x, df.y)
        radius = 3 * self.factor
        df.R = (df.R+40).clip(upper=255)
        df.G = (df.G+40).clip(upper=255)
        df.B = (df.B+40).clip(upper=255)
        rgb = list(zip(df.R, df.G, df.B))
        for i, color in enumerate(rgb):
            c = self.create_circle(self.canvas, x[i], y[i], radius)
            colorstring = '#%02x%02x%02x' % color
            self.canvas.itemconfig(c, fill=(colorstring))

    def add_hexagons(self, hexagons):
        l = 4.88
        for hexagon in hexagons:
            a, b = hexagon.center_coords 
            hex_df = pd.DataFrame({
                'a': [a-l, a,   a+l, a+l, a,   a-l],
                'b': [b+l, b+l, b,   b-l, b-l, b  ]
            })
            hex_df = self.calc_xy(hex_df)
            x, y = self.transform_xy(hex_df.x, hex_df.y)
            self.canvas.create_polygon(x[0], y[0], x[1], y[1], x[2], y[2], x[3], y[3], x[4], y[4], x[5], y[5], outline='#4a3209', width=3*self.factor)

    def update(self):
        self.canvas.delete('all')
        df = self.get_dataframe()
        self.update_transformation(df)
        self.add_hexagons(self.board.hexagons)
        self.add_leds(df)
        self.canvas.update()
        self.win.update()


if __name__ == '__main__':
    from board import Board
    board = Board()
    gui = GUI(board)
