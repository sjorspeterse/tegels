'''
Based on:
https://zvold.blogspot.com/2010/01/bresenhams-line-drawing-algorithm-on_26.html

Interesting references:
http://members.chello.at/~easyfilter/bresenham.html
https://www.redblobgames.com/grids/hexagons
'''

from collections import namedtuple
from hexmath import hex_linedraw, Hex_ab

class Draw:

    Point = namedtuple('Point', ['a', 'b'])

    @classmethod
    def new_line_algorithm(lcs, board, p0: Point, p1: Point, rgb):
        hex0 = Hex_ab(p0.a, p0.b)
        hex1 = Hex_ab(p1.a, p1.b)
        path = hex_linedraw(hex0, hex1)
        for hex in path:
            board.set_pixel(hex.r, hex.q, rgb[0], rgb[1], rgb[2])


    @classmethod
    def line_ab(cls, board, p0: Point, p1: Point):
        if p1.a - p0.a > 0:
            cls._line_ab_horizontal(board, p0, p1)

    @classmethod
    def _line_ab_horizontal(cls, board, p0, p1):
        two_deltaX = 2*(p1.a-p0.a) + p1.b - p0.b
        deltaY = p1.b - p0.b

        a = p0.a
        b = p0.b
        error=0
        while True:
            board.set_pixel(a, b, 150, 150, 150)
            if a == p1.a and b == p1.b: break
            error += 3*deltaY
            if error > two_deltaX:
                b += 1
                error -= 3*two_deltaX
            else:
                a += 1
                error += 3*deltaY
