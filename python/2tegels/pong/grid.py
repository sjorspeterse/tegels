class Grid:
    '''Represents an infinite grid with coordinates a and b. The 'a' coordinates are steps to the right,
    the 'b' coordinates are steps to the upper-right at a 60-degree angle.'''
    
    def __init__(self):
        self.grid = {}  # (a, b) => (R, G, B)

    def clear(self):
        self.grid = {} 
    
    def get_pixel(self, a, b):
        return self.grid.get((a, b), (0, 0, 0))

    def set_pixel(self, a, b, R, G, B):
        self.grid[(a, b)] = (R, G, B)