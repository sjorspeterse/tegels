'''
Interface for the game. Delegates most work to other classes.
'''

import numpy as np
from math import sqrt

from grid import Grid
from mainloop import MainLoop
from hexconfigurator import HexConfigurator
from drawing import Draw

INTER_DISTANCE = 12.5 # mm

class Board:
    def __init__(self, simulated=False):
        self.grid = Grid()
        self.hexagons = []
        self.mainloop = MainLoop(self, simulated)
        self.hexconfig = HexConfigurator(self)
        firsthex = self.hexconfig.add_first_hexagon(id=1)
        secondhex = self.hexconfig.add_hexagon(id=2, neighbor=firsthex, neighbor_side=2, new_side=5)
        thirdhex = self.hexconfig.add_hexagon(id=3, neighbor=secondhex, neighbor_side=4, new_side=5)
        fourthhex = self.hexconfig.add_hexagon(id=4, neighbor=secondhex, neighbor_side=0, new_side=2)
        fifthhex = self.hexconfig.add_hexagon(id=5, neighbor=thirdhex, neighbor_side=5, new_side=2)
        sixthhex = self.hexconfig.add_hexagon(id=6, neighbor=fifthhex, neighbor_side=0, new_side=2)
        seventhhex = self.hexconfig.add_hexagon(id=7, neighbor=sixthhex, neighbor_side=1, new_side=2)

    def clear(self):
        self.grid.clear()
    
    def set_pixel(self, a, b, R, G, B):
        self.grid.set_pixel(a, b, R, G, B)
    
    def get_pixel(self, a, b):
        return self.grid.get_pixel(a, b)

    def get_hexagon(self, id):
        for hexagon in self.hexagons:
            if hexagon.id == id:
                return hexagon
        raise Exception('Hexagon', id, 'does not exist')

    def draw_line_ab(self, a0, b0, a1, b1):
        p0 = Draw.Point(a0, b0)
        p1 = Draw.Point(a1, b1)
        Draw.line_ab(self, p0, p1)

    def draw_line_new(self, a0, b0, a1, b1, rgb):
        p0 = Draw.Point(a0, b0)
        p1 = Draw.Point(a1, b1)
        Draw.new_line_algorithm(self, p0, p1, rgb)

    def start(self):
        self.mainloop.start()

    @classmethod
    def ab2xy(cls, ab):
        '''Transforms an a/b Nx2 matrix into an x/y Nx2 matrix.'''
        transformation_matrix = np.array([[1, 0], [0.5, 0.5*sqrt(3)]]) * INTER_DISTANCE
        return ab @ transformation_matrix

    @classmethod
    def xy2ab(cls, xy):
        '''Transforms an x/y Nx2 matrix into an a/b Nx2 matrix.'''
        transformation_matrix = np.array([[1, 0], [-sqrt(3)/3, 2/3*sqrt(3)]]) / INTER_DISTANCE
        return xy @ transformation_matrix
