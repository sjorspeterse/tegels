'''
Main loop of the game. Responsible for:
- Callbacks
- Animations
- Serial communication
'''
from time import sleep
from callbacks import Callbacks
from simserial import SimSerial
from hwserial import HWSerial

class MainLoop:
    def __init__(self, board, simulated=False):
        self.callbacks = Callbacks()
        if simulated:
            self.serial = SimSerial(board)
        else:
            self.serial = HWSerial(board)
        self.board = board

    def start(self):
        self.loop()

    def updateHexagonsFromGrid(self):
        for hexagon in self.board.hexagons:
            for (a, b) in hexagon.coordinates:
                (R, G, B) = self.board.get_pixel(a, b)
                hexagon.set_pixel(a, b, R, G, B)


    def loop(self):
        while True:
            self.callbacks.onMainLoopStart()
            self.updateHexagonsFromGrid() 
            self.serial.write()
            # todo: apply animations + serial read + parse inputs + update hexconfig


            sleep(1/120)
