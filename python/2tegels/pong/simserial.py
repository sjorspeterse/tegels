'''
Simulated serial port
'''

from gui import GUI

class SimSerial:
    def __init__(self, board) -> None:
        self.board = board
        self.gui = GUI(board)

    def write(self):
        self.gui.update()