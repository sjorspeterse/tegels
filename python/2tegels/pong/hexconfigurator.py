'''
Manages the different hexagon configurations
'''

from hexagon import Hexagon

class HexConfigurator:
    def __init__(self, board) -> None:
        self.board = board
    
    def add_first_hexagon(self, id) -> Hexagon:
        hexagon = Hexagon(self.board, id)
        self.board.hexagons.append(hexagon)
        return hexagon
    
    def add_hexagon(self, id, neighbor, neighbor_side, new_side) -> Hexagon:
        ''' Add a new hexagon to an exiting one.'''
        rotations = (neighbor_side - new_side + 3) % 6

        if neighbor_side == Hexagon.NORTH:
            translate_a, translate_b = -5, 10
        elif neighbor_side == Hexagon.NORTHWEST:
            translate_a, translate_b = -10, 5
        elif neighbor_side == Hexagon.SOUTHWEST:
            translate_a, translate_b = -5, -5
        elif neighbor_side == Hexagon.SOUTH:
            translate_a, translate_b = 5, -10
        elif neighbor_side == Hexagon.SOUTHEAST:
            translate_a, translate_b = 10, -5
        elif neighbor_side == Hexagon.NORTHEAST:
            translate_a, translate_b = 5, 5
        else:
            raise Exception('HexConfigurator::add_hexagon: invalid side')

        translate_a += neighbor.center_coords[0]
        translate_b += neighbor.center_coords[1]

        rotated = Hexagon(self.board, id) \
            .rotate(rotations) \
            .translate(translate_a, translate_b)
        self.board.hexagons.append(rotated)
        return rotated