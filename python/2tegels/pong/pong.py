'''Simulates the Pong game. Uses software architecture v2.'''

from math import sqrt, sin, cos, exp, floor, ceil
import time
import numpy as np
from board import Board
from argparse import ArgumentParser

def roi_circle(x, y, r):
    '''Calculates the region of interest for a circle. This is a tight box around
    the circle in the ab coordinates system.'''
    leftx = x - r*0.5*sqrt(3)
    lefty = y + r*0.5
    ab_left = Board.xy2ab(np.array([[leftx, lefty]]))

    rightx = x + r*0.5*sqrt(3)
    righty = y - r*0.5
    ab_right = Board.xy2ab(np.array([[rightx, righty]]))

    ab_top = Board.xy2ab(np.array([[0, y+r]]))
    ab_bottom = Board.xy2ab(np.array([[0, y-r]]))

    a_min = ceil(ab_left[0, 0])
    a_max = floor(ab_right[0, 0])
    b_min = ceil(ab_bottom[0, 1])
    b_max = floor(ab_top[0, 1])
    roi = [[a, b] for b in range(b_min, b_max+1) for a in range(a_min, a_max+1)]
    return roi

def add_circle(board, x, y, r):
    '''Add a circle to the frame with center (x, y) and radius r, all in mm.'''
    roi = roi_circle(x, y, r)
    xy = Board.ab2xy(roi)
    for xy_led, ab_led in zip(xy, roi):
        x_led, y_led = xy_led
        deltax = x_led - x
        deltay = y_led - y
        dist = sqrt(deltax*deltax + deltay*deltay)
        var = r*2.5
        value = int(255 * exp(-0.5*dist*dist/var))
        a, b = ab_led
        board.set_pixel(a=a, b=b, R=value, G=0, B=0)


class Pong:
    def __init__(self, args):
        self.board = Board(args.simulate)

    def onMainLoopStart(self):
        self.board.clear()
        t = time.time()
        a = int(4*(sin(2*t)))
        b = int(4*(cos(6*t)))
        x = -60*(sin(t))-30
        y = -60*(cos(3*t))-30
             
        # self.board.set_pixel(a=a, b=b, R=0, G=255, B=0)
        # add_circle(self.board, x, y, 20)
        # self.board.draw_line_ab(-4, -2, -3, 9)
        # self.board.draw_line_ab(0, 0, -b, -a)
        from hexmath import Layout, layout_pointy, Point, pixel_to_hex, hex_round
        layout = Layout(layout_pointy, Point(1, 1), Point(0, 0))

        major_clock_x = 20*cos(6*t)
        major_clock_y = 20*sin(6*t)
        minor_clock_x = 10*cos(0.5*t)
        minor_clock_y = 10*sin(0.5*t)
        major_hex = pixel_to_hex(layout, Point(major_clock_x, major_clock_y))
        major_hex = hex_round(major_hex)
        minor_hex = pixel_to_hex(layout, Point(minor_clock_x, minor_clock_y))
        minor_hex = hex_round(minor_hex)
        # print('clock', clock_x, clock_y)
        # print('hex', hex.r, hex.q)

        # self.board.draw_line_new(0, 0, -b, -a)
        self.board.draw_line_new(0, 0, major_hex.r, major_hex.q, [150, 150, 150])
        self.board.draw_line_new(0, 0, minor_hex.r, minor_hex.q, [150, 0, 0])
        # self.board.set_pixel(-2, -6, 255, 0, 0)
        # self.board.set_pixel(2, 5, 255, 0, 0)
    
    def start(self):
        self.board.mainloop.callbacks.onMainLoopStart = self.onMainLoopStart
        self.board.start()

def main():
    parser = ArgumentParser(description='A contemporary version of the classic Pong game')
    parser.add_argument('-s', '--simulate', action='store_true', help='Don\'t write data to the serial port, but simulate game with a GUI')
    args = parser.parse_args()

    pong = Pong(args)
    pong.start()

if __name__ == '__main__':
    main()
