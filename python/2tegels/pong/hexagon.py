'''
Represents a single Hexagon
'''

import numpy as np
import pandas as pd

INDICES = [
             0,  1,  2, 3,  4,
          10,  9,  8,  7,  6,  5,
       11,  12, 13, 14, 15, 16, 17,
      25, 24, 23, 22, 21, 20, 19, 18,
    26, 27, 28, 29, 30, 31, 32, 33, 34,
      42, 41, 40, 39, 38, 37, 36, 35,
        43, 44, 45, 46, 47, 48, 49,
          55, 54, 53, 52, 51, 50,
            56, 57, 58, 59, 60
]

A_COORDINATES = [
            -4, -3, -2, -1,  0,
          -4, -3, -2, -1,  0,  1,
        -4, -3, -2, -1,  0,  1,  2,
      -4, -3, -2, -1,  0,  1,  2,  3,
    -4, -3, -2, -1,  0,  1,  2,  3,  4,
      -3, -2, -1,  0,  1,  2,  3,  4,
        -2, -1,  0,  1,  2,  3,  4,
          -1,  0,  1,  2,  3,  4,
             0,  1,  2,  3,  4
]

B_COORDINATES = [
             4,  4,  4,  4,  4,
           3,  3,  3,  3,  3,  3,
         2,  2,  2,  2,  2,  2,  2,
       1,  1,  1,  1,  1,  1,  1,  1,
     0,  0,  0,  0,  0,  0,  0,  0,  0,
      -1, -1, -1, -1, -1, -1, -1, -1,
        -2, -2, -2, -2, -2, -2, -2,
          -3, -3, -3, -3, -3, -3,
            -4, -4, -4, -4, -4
]

''' UNIT DIRECTIONS
    C(-1,1)   B(0,1)

D(-1,0)   O(0,0)   A(1,0)

    E(0,-1)   F(1,-1)
'''

UNIT_VECTORS = [
    [ 1,  0],  # A
    [ 0,  1],  # B
    [-1,  1],  # C
    [-1,  0],  # D
    [ 0, -1],  # E
    [ 1, -1],  # F
    [ 1,  0]   # A
]

NUM_PIXELS = len(INDICES)
NUM_COLORS = NUM_PIXELS * 3

class Hexagon:
    def __init__(self, board, id) -> None:
        self.id = id
        self.data = [0] * NUM_COLORS
        self.board = board

        self.coordinates = np.array([A_COORDINATES, B_COORDINATES]).T
        self._lookup = {(a, b): i for a, b, i in zip(self.coordinates[:, 0], self.coordinates[:, 1], INDICES)}
        self.center_coords = self.coordinates[30]

    NORTH       = 0
    NORTHWEST   = 1
    SOUTHWEST   = 2
    SOUTH       = 3
    SOUTHEAST   = 4
    NORTHEAST   = 5

    def set_data(self, i, R, G, B):
        self.data[i*3] = R 
        self.data[i*3+1] = G 
        self.data[i*3+2] = B 

    def set_pixel(self, a, b, R, G, B):
        index = self._lookup[(a, b)]
        self.set_data(index, R, G, B)

    def translate(self, a, b):
        offset = np.array([
          [a]*NUM_PIXELS,
          [b]*NUM_PIXELS
        ]).T
        self.coordinates += offset
        self._lookup = {(a, b): i for a, b, i in zip(self.coordinates[:, 0], self.coordinates[:, 1], INDICES)}
        self.center_coords = (self.center_coords[0]+a, self.center_coords[1]+b)
        return self

    def rotate(self, rotations):
        '''Rotates the hexagon in steps of 60 degrees counter-clockwise''' 
        rotations = rotations % 6
        a_center, b_center = self.center_coords

        M = self.coordinates    # Original matrix
        O = np.array([          # Offset matrix
          [a_center]*NUM_PIXELS,
          [b_center]*NUM_PIXELS
        ]).T
        R = np.array([          # Rotation matrix
            UNIT_VECTORS[rotations],
            UNIT_VECTORS[rotations+1]
        ])
        self.coordinates = (M-O)@R+O
        self._lookup = {(a, b): i for a, b, i in zip(self.coordinates[:, 0], self.coordinates[:, 1], INDICES)}
        return self

    def get_dataframe(self):
        tuples = sorted(zip(INDICES, self.coordinates[:, 0], self.coordinates[:, 1]))
        _, a, b = zip(*tuples)
        R, G, B = np.reshape(self.data, (61, 3)).T
        df = pd.DataFrame({'a': a, 'b': b, 'R': R, 'G': G, 'B': B})
        return df
