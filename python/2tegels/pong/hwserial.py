'''
Send data over the serial connection
'''

import serial
from time import sleep

BAUD_RATE = 1000000
SERIAL_PORT = '/dev/ttyUSB0'

START_MESSAGE = 255
MASTER_ADDR = 0
SLEEP_TIME = 0.0020 # ms

class HWSerial:
    def __init__(self, board) -> None:
        self.sp = serial.Serial(SERIAL_PORT, BAUD_RATE, timeout = 0)
        self.sp.flush()
        self.sp.flushInput()
        self.sp.flushOutput()
        self.board = board

    def write(self):
        # this should be done on a separate thread
        for hexagon in self.board.hexagons:
            self.sp.write([START_MESSAGE, MASTER_ADDR, hexagon.id, len(hexagon.data)] + hexagon.data)
            sleep(SLEEP_TIME)
