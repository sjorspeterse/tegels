import pandas as pd 

FILE_NAME = 'tileconfig.csv'

tile1 = {
    'number': 0,
    'northTile': -1,
    'northEastTile': -1,
    'southEastTile': 1,
    'southTile': -1,
    'southWestTile': -1,
    'northWestTile': -1,
    'northCurrent': 0.001,
    'northEastCurrent': -0.001,
    'southEastCurrent': 0.150,
    'southCurrent': 0.001,
    'southWestCurrent': -0.001,
    'northWestCurrent': 0.000,
}

tile2 = {
    'number': 1,
    'northTile': -1,
    'northEastTile': -1,
    'southEastTile': 1,
    'southTile': 0,
    'southWestTile': -1,
    'northWestTile': -1,
    'northCurrent': 0.001,
    'northEastCurrent': -0.001,
    'southEastCurrent': 0.002,
    'southCurrent': -0.151,
    'southWestCurrent': -0.001,
    'northWestCurrent': 0.000,
}

def main():
    config = pd.DataFrame([tile1, tile2])
    config.to_csv(FILE_NAME, index=False) 
if __name__ == '__main__':
    main()