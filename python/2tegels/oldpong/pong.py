'''Simulates the Pong game. This is the old version using software architecture v1.'''

from math import sqrt, sin, cos, exp, floor, ceil
from time import sleep
import numpy as np
from frame import Frame

def roi_circle(x, y, r):
    '''Calculates the region of interest for a circle. This is a tight box around
    the circle in the ab coordinates system.'''
    leftx = x - r*0.5*sqrt(3)
    lefty = y + r*0.5
    ab_left = Frame.xy2ab(np.array([[leftx, lefty]]))

    rightx = x + r*0.5*sqrt(3)
    righty = y - r*0.5
    ab_right = Frame.xy2ab(np.array([[rightx, righty]]))

    ab_top = Frame.xy2ab(np.array([[0, y+r]]))
    ab_bottom = Frame.xy2ab(np.array([[0, y-r]]))

    a_min = ceil(ab_left[0, 0])
    a_max = floor(ab_right[0, 0])
    b_min = ceil(ab_bottom[0, 1])
    b_max = floor(ab_top[0, 1])
    roi = [[a, b] for b in range(b_min, b_max+1) for a in range(a_min, a_max+1)]
    return roi

def add_circle(frame, x, y, r):
    '''Add a circle to the frame with center (x, y) and radius r, all in mm.'''
    roi = roi_circle(x, y, r)
    xy = Frame.ab2xy(roi)
    for xy_led, ab_led in zip(xy, roi):
        x_led, y_led = xy_led
        deltax = x_led - x
        deltay = y_led - y
        dist = sqrt(deltax*deltax + deltay*deltay)
        var = r*2.5
        value = int(255 * exp(-0.5*dist*dist/var))
        a, b = ab_led
        frame.set_pixel(a=a, b=b, R=value, G=0, B=0)

def main():
    '''Main loop of the pong game.'''
    t = 0
    frame = Frame()
    while True:
        frame.clear()
        t += 0.1
        a = int(4*(sin(t)))
        b = int(4*(cos(3*t)))
        x = -40*(sin(t))
        y = -40*(cos(3*t))
             
        frame.set_pixel(a=a, b=b, R=0, G=255, B=0)
        add_circle(frame, x, y, 20)
        frame.write()
        sleep(1/30)


if __name__ == '__main__':
    main()
