import sys
import time
import logging
from watchdog.observers import Observer
from watchdog import events
import pandas as pd
import pickle

FILE_NAME = 'framebuffer.csv'

def newFrame():
    try:
        dataframe = pd.read_csv(FILE_NAME, index_col='id')
    except:
        return

    result = dataframe.to_dict(orient='list')
    print(str(result).replace(' ',''), flush=True)

class FrameBufferHandler(events.FileSystemEventHandler):
    def on_modified(self, event):
        if event.is_directory:
            return
        newFrame()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    path = sys.argv[1] if len(sys.argv) > 1 else '.'
    event_handler = FrameBufferHandler()

    newFrame()
    observer = Observer()
    observer.schedule(event_handler, FILE_NAME)
    observer.start()
    try:
        while True:
            time.sleep(1)
    # except KeyboardInterrupt:
    except:
        observer.stop()
    observer.join()
