'''Visualize the stream of data coming in.'''
# TODO: put exact data format in module docstring
import select
import sys
import signal
import tkinter as tk
import pandas as pd
import numpy as np
from frame import Frame


def create_circle(canvas, x, y, r):
    '''Create a single circle/LED and add to the canvas.'''
    x0 = x - r
    y0 = y - r
    x1 = x + r
    y1 = y + r
    return canvas.create_oval(x0, y0, x1, y1)

def interrupt_handler(signum, frame):
    print('Exiting')
    sys.exit(1)

def calc_xy(df):
    '''Add x and y coordinates to the dataframe.'''
    ab = np.array([df.a, df.b]).T
    xy = Frame.ab2xy(ab)
    df['x'] = xy[:, 0]
    df['y'] = xy[:, 1]
    return df

def new_frame(line, canvas):
    '''Show a new frame on the screen.'''
    df = pd.DataFrame(eval(line))
    df = calc_xy(df)
    canvas.delete('all')
    x, y = df.x, df.y
    x -= (min(x) + max(x))/2  # Let xmin = -xmax (centered) 
    y -= (min(y) + max(y))/2
    width, height = canvas.winfo_width(), canvas.winfo_height()
    xdiff, ydiff = max(x) - min(x), max(y) - min(y)
    xfactor, yfactor = width / xdiff, height / ydiff
    factor = min(xfactor, yfactor) * 0.9  # Add margin
    radius = 3 * factor
    rgb = list(zip(df.R, df.G, df.B))
    for i, color in enumerate(rgb):
        xi = x[i]*factor + width/2
        yi = -y[i]*factor + height/2
        c = create_circle(canvas, xi, yi, radius)
        colorstring = '#%02x%02x%02x' % color
        canvas.itemconfig(c, fill=(colorstring))
    canvas.update()

def read_input(canvas):
    '''Read the input from the input pipe'''
    while sys.stdin in select.select([sys.stdin], [], [], 0)[0]:
        line = sys.stdin.readline()
        if line:
            new_frame(line, canvas)
        else: 
            print('eof')
            sys.exit(0)

def check_input(canvas):
    '''Poll if there is new input. Also call this method again in the future.'''
    read_input(canvas)
    canvas.after(1, lambda: check_input(canvas))

def main():
    '''Main loop of the visualizer.'''
    signal.signal(signal.SIGINT, interrupt_handler)

    win = tk.Tk()
    win.geometry('1000x800')
    win.title('Nutteloze zeshoek')

    canvas = tk.Canvas(win)
    canvas.configure(bg='black')
    canvas.pack(fill=tk.BOTH, expand=True)
    win.update_idletasks()


    canvas.after(0, lambda: check_input(canvas))
    win.mainloop()


if __name__ == '__main__':
    main()
