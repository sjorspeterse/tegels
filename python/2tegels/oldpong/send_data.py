import sys
from time import sleep
import logging
from watchdog.observers import Observer
from watchdog import events
import pandas as pd
import numpy as np

import serial

FILE_NAME = 'framebuffer.csv'
SERIAL_PORT = '/dev/ttyUSB0'
BAUD_RATE = 1000000
MASTER_ADDR = 0
START_MESSAGE = 255
SLEEP_TIME = 0.0020 # ms
NUM_COLORS = 61*3



class Tile:
    def __init__(self):
        self.index = 1
        self.data = [0] * NUM_COLORS

    def set_data(self, data):
        self.data = [254 if i == 255 else i for i in data]

    def set_data(self, i, r, g, b):
        self.data[i*3] = r if r < 255 else 254
        self.data[i*3+1] = g if g < 255 else 254
        self.data[i*3+2] = b if b < 255 else 254


class LedStream:
    def __init__(self):
        self.sp = serial.Serial(SERIAL_PORT, BAUD_RATE, timeout = 0)
        self.sp.flush()
        self.sp.flushInput()
        self.sp.flushOutput()
        self.tiles = []
        tile = Tile()
        self.add_tile(tile)

    def add_tile(self, tile):
        self.tiles.append(tile)

    def send(self):
        for tile in self.tiles:
            self.sp.write([START_MESSAGE, MASTER_ADDR, tile.index, len(tile.data)] + tile.data)
            sleep(SLEEP_TIME)
            tile.data = [0] * NUM_COLORS

ledstream = LedStream()

def newFrame():
    try:
        df = pd.read_csv(FILE_NAME, index_col='id')
    except Exception as e:
        print(e)
        return

    result = df[(df.index>=0) & (df.index <61)]
    data = list(zip(result.index, result.R, result.G, result.B))
    for i, r, g, b in data:
        ledstream.tiles[0].set_data(i, r, g, b)

    ledstream.send()

class FrameBufferHandler(events.FileSystemEventHandler):
    def on_modified(self, event):
        if not isinstance(event, events.FileModifiedEvent):
            return
        newFrame()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    path = sys.argv[1] if len(sys.argv) > 1 else '.'
    event_handler = FrameBufferHandler()

    observer = Observer()
    observer.schedule(event_handler, FILE_NAME)
    observer.start()
    try:
        while True:
            sleep(1)
    except KeyboardInterrupt:
        observer.stop()

    observer.join()

