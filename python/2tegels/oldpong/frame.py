import pandas as pd
import numpy as np
from math import sqrt

INDICES = [
             0,  1,  2, 3,  4,
          10,  9,  8,  7,  6,  5,
       11,  12, 13, 14, 15, 16, 17,
      25, 24, 23, 22, 21, 20, 19, 18,
    26, 27, 28, 29, 30, 31, 32, 33, 34,
      42, 41, 40, 39, 38, 37, 36, 35,
        43, 44, 45, 46, 47, 48, 49,
          55, 54, 53, 52, 51, 50,
            56, 57, 58, 59, 60
]

A_COORDINATES = [
            -4, -3, -2, -1,  0,
          -4, -3, -2, -1,  0,  1,
        -4, -3, -2, -1,  0,  1,  2,
      -4, -3, -2, -1,  0,  1,  2,  3,
    -4, -3, -2, -1,  0,  1,  2,  3,  4,
      -3, -2, -1,  0,  1,  2,  3,  4,
        -2, -1,  0,  1,  2,  3,  4,
          -1,  0,  1,  2,  3,  4,
             0,  1,  2,  3,  4
]

B_COORDINATES = [
             4,  4,  4,  4,  4,
           3,  3,  3,  3,  3,  3,
         2,  2,  2,  2,  2,  2,  2,
       1,  1,  1,  1,  1,  1,  1,  1,
     0,  0,  0,  0,  0,  0,  0,  0,  0,
      -1, -1, -1, -1, -1, -1, -1, -1,
        -2, -2, -2, -2, -2, -2, -2,
          -3, -3, -3, -3, -3, -3,
            -4, -4, -4, -4, -4
]

''' UNIT DIRECTIONS
    C(-1,1)   B(0,1)

D(-1,0)   O(0,0)   A(1,0)

    E(0,-1)   F(1,-1)
'''

UNIT_VECTORS = [
    [ 1,  0],  # A
    [ 0,  1],  # B
    [-1,  1],  # C
    [-1,  0],  # D
    [ 0, -1],  # E
    [ 1, -1],  # F
    [ 1,  0]   # A
]

NORTH       = 0
NORTHWEST   = 1
SOUTHWEST   = 2
SOUTH       = 3
SOUTHEAST   = 4
NORTHEAST   = 5

FILE_NAME = 'framebuffer.csv'
NUM_PIXELS = 61
INTER_DISTANCE = 12.5 # mm
CENTER_PIXEL = 30


class Tile:
    """ Represents a physical hexagon """

    def __init__(self, tile_id, framebuffer=None):
        self.ids = np.array(INDICES) + (tile_id*NUM_PIXELS)
        if framebuffer is None:
            self.abmatrix = np.array([A_COORDINATES, B_COORDINATES]).T

    def get_dataframe(self) -> pd.DataFrame:
        return pd.DataFrame({
            'id': self.ids,
            'a': self.abmatrix[:, 0],
            'b': self.abmatrix[:, 1],
        }).set_index('id')

    def rotate(self, rotations):
        '''Rotates the tile in steps of 60 degrees counter-clockwise''' 
      # TODO ->  (M-O)@R+O, M=original matrix, R= rotation matrix, O=Offset
        rotations = rotations % 6
        rotation_matrix = np.array([
            UNIT_VECTORS[rotations],
            UNIT_VECTORS[rotations+1]
        ])
        self.abmatrix = self.abmatrix @ rotation_matrix
        return self

    # TODO get_rotation(tile_id)  get (P31-P30) and compare with UNIT_VECTORS
    # TODO get_offset(tile_id) -> (a, b) of P30

class Frame:
    '''Represents an infinite grid with coordinates a and b. The 'a' coordinates are steps to the right,
    the 'b' coordinates are steps to the upper-right at a 60-degree angle.'''
    def __init__(self):
        self.grid = {}  # (a, b) => (R, G, B)
        self.framebuffer = Tile(0).get_dataframe()
        self.framebuffer['R'] = [0] * NUM_PIXELS
        self.framebuffer['G'] = [0] * NUM_PIXELS
        self.framebuffer['B'] = [0] * NUM_PIXELS

    def add_tile(self, neighbor_id, new_id, neighbor_side, new_side) -> None:
        # TODO updates framebuffer
        rotations = (neighbor_side - new_side + 3) % 6
        rotated = Tile(new_id).rotate(rotations)
        # TODO Next up: Translate!
        old_centerpixel = self.framebuffer.loc[neighbor_id*61+CENTER_PIXEL]

    @classmethod
    def ab2xy(cls, ab):
        '''Transforms an a/b Nx2 matrix into an x/y Nx2 matrix.'''
        transformation_matrix = np.array([[1, 0], [0.5, 0.5*sqrt(3)]]) * INTER_DISTANCE
        return ab @ transformation_matrix

    @classmethod
    def xy2ab(cls, xy):
        '''Transforms an x/y Nx2 matrix into an a/b Nx2 matrix.'''
        transformation_matrix = np.array([[1, 0], [-sqrt(3)/3, 2/3*sqrt(3)]]) / INTER_DISTANCE
        return xy @ transformation_matrix

    def remove_tile(self, frame_id) -> None:
        # TODO updates framebuffer
        pass

    def set_pixel(self, a, b: int, R, G, B: int) -> None:
        self.grid[(a, b)] = (R, G, B)

    def clear(self) -> None: 
        '''Clears the frame'''
        self.grid = {} 
        self.framebuffer['R'] = 0
        self.framebuffer['G'] = 0
        self.framebuffer['B'] = 0

    def _merge_grid_framebuffer(self):
        for a, b in self.grid:  
            filter = (self.framebuffer['a'] == a) & (self.framebuffer['b'] == b)
            R, G, B = self.grid[(a, b)]

            if sum(filter) == 0:
                newrowId = len(self.framebuffer.index)
                newRow = pd.DataFrame({'id': [-1], 'a': [a], 'b': [b], 'R': [R], 'G': [G], 'B': [B]}).set_index('id')
                self.framebuffer = self.framebuffer.append(newRow)
                continue

            self.framebuffer.loc[filter, 'R'] = R
            self.framebuffer.loc[filter, 'G'] = G
            self.framebuffer.loc[filter, 'B'] = B


    def write(self) -> None:
        self._merge_grid_framebuffer()
        csv_str = self.framebuffer.to_csv()
        with open(FILE_NAME, 'w') as f:
            f.write(csv_str)
 
def fill_pixels(myFrame):
    myFrame.set_pixel(a=0, b=0, R=255, G=0, B=0)
    myFrame.set_pixel(a=1, b=0, R=255, G=0, B=0)
    myFrame.set_pixel(a=0, b=1, R=255, G=255, B=0)
    myFrame.set_pixel(a=-1, b=1, R=255, G=0, B=0)
    myFrame.set_pixel(a=-1, b=0, R=255, G=0, B=0)
    myFrame.set_pixel(a=0, b=-1, R=255, G=0, B=0)
    myFrame.set_pixel(a=1, b=-1, R=255, G=0, B=0)
    myFrame.set_pixel(a=4, b=0, R=0, G=255, B=0)
    myFrame.set_pixel(a=0, b=4, R=0, G=255, B=0)
    myFrame.set_pixel(a=-4, b=4, R=0, G=255, B=0)
    myFrame.set_pixel(a=-4, b=0, R=0, G=255, B=0)
    myFrame.set_pixel(a=0, b=-4, R=0, G=255, B=0)
    myFrame.set_pixel(a=4, b=-4, R=0, G=255, B=0)
    myFrame.set_pixel(a=-10, b=0, R=0, G=0, B=255)


def test():
    myFrame = Frame()
    fill_pixels(myFrame)
    myFrame.add_tile(0, 1, SOUTH, NORTHEAST)
    myFrame.write()

if __name__ == '__main__':
    test()
