'''Stream LED data: No communication back from the tiles'''
SERIAL_PORT = '/dev/ttyUSB0'
BAUD_RATE = 1000000
MASTER_ADDR = 0
START_MESSAGE = 255
SLEEP_TIME = 0.0020 # ms
NUM_COLORS = 61*3

import serial
from time import sleep

from data.MANDELBROT import frames as video1
from data.KNIKKERS import frames as video2
from data.FLOW import frames as video3
from data.PULSE import frames as video4


class Tile:
    def __init__(self, index):
        self.index = index
        self.data = [0] * NUM_COLORS

    def set_data(self, data):
        self.data = [254 if i == 255 else i for i in data]


class LedStream:
    def __init__(self):
        self.sp = serial.Serial(SERIAL_PORT, BAUD_RATE, timeout = 0)
        self.sp.flush()
        self.sp.flushInput()
        self.sp.flushOutput()
        self.tiles = []
        tile1 = Tile(1)
        tile2 = Tile(2)
        self.add_tile(tile1)
        self.add_tile(tile2)

    def add_tile(self, tile):
        self.tiles.append(tile)

    def send(self):
        for tile in self.tiles:
            self.sp.write([START_MESSAGE, MASTER_ADDR, tile.index, len(tile.data)] + tile.data)
            sleep(SLEEP_TIME)

def play_video():
    ledstream = LedStream()
    while(1):
        for video in [video1, video2, video3, video4]:
            #for frame in video:
            for i, frame in enumerate(video):
                ledstream.tiles[0].set_data(frame)
                ledstream.tiles[1].set_data(video[-i])
                ledstream.send()
                sleep(1/60)


if __name__ == '__main__':
    play_video()
