from collections import namedtuple 
from math import sin, cos, pi, sqrt

LEDS_PER_SIDE = 5
INTER_DISTANCE = 1.25

Point = namedtuple('Point', ['x', 'y'], defaults=[0, 0])

def calc_xy():
    xlist = []
    ylist = []
    TOP_LEFT = Point(4*INTER_DISTANCE*cos(pi*2/3), -4*INTER_DISTANCE*sin(pi*2/3))
    cur_point = TOP_LEFT
    deltay = 0.5*sqrt(3)*INTER_DISTANCE
    increment = INTER_DISTANCE
    for row in range(LEDS_PER_SIDE-1):
        for _ in range(LEDS_PER_SIDE + row):
            xlist.append(cur_point.x)
            ylist.append(cur_point.y)
            cur_point = Point(cur_point.x + increment, cur_point.y)
        increment = -increment
        cur_point = Point(cur_point.x + 0.5*increment, cur_point.y + deltay)
    for row in range(LEDS_PER_SIDE):
        for _ in range(2*LEDS_PER_SIDE - 1 - row):
            xlist.append(cur_point.x)
            ylist.append(cur_point.y)
            cur_point = Point(cur_point.x + increment, cur_point.y)
        increment = -increment
        cur_point = Point(cur_point.x + 1.5*increment, cur_point.y + deltay)
    return xlist, ylist
