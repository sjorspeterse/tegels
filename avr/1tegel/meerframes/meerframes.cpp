#define NUM_PIXELS 61

#include <util/delay.h>
#include <avr/power.h>
#include <avr/pgmspace.h>         //needed for PROGMEM
#include "leds.h"

#include "MANDELBROT.h"
// #include "KNIKKERS.h"
// #include "FLOW.h"
// #include "PULSE.h"

Ledstrip ledstrip(50);

static uint32_t Color(uint8_t r, uint8_t g, uint8_t b) {
  return ((uint32_t)r << 16) | ((uint32_t)g <<  8) | b;
}

uint32_t Pixel(uint8_t index, int frame) {
   uint8_t red, green, blue;
   int color_addr = (int) LED_DATA + 61*3*frame + index*3;
   red = pgm_read_byte_near(color_addr+0);
   green = pgm_read_byte_near(color_addr+1);
   blue = pgm_read_byte_near(color_addr+2);
   return Color(red, green, blue);
}

void setPixelColor(int index, uint32_t color) {
  uint8_t r = color >> 16 & 0xff;
  uint8_t g = color >> 8 & 0xff;
  uint8_t b = color & 0xff;
  ledstrip.set_pixel_color(index, r, g, b);
}

void showFrame(int frame) {
    for(int i=0; i<NUM_PIXELS; i++) {
        setPixelColor(i, Pixel(i, frame));
    }
    ledstrip.show();
}

void setup() {
    // clock_prescale_set(clock_div_1);    // set clock to 8 MHz
}

void loop() {
    for(int frame_nr=0; frame_nr<30; frame_nr++) {
        showFrame(frame_nr);
        _delay_ms(33);
    }
}

int main() {
    setup();
    while(1) {
        loop();
    }
    return 0;
}
