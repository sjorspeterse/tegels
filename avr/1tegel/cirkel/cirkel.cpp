#define NUM_PIXELS 61
// Max brightness is 255
#define BRIGHTNESS 255

#include <util/delay.h>
#include <avr/power.h>
#include <avr/pgmspace.h>         //needed for PROGMEM
#include <math.h>
#include "leds.h"

Ledstrip ledstrip(255);

const char x_list[NUM_PIXELS] PROGMEM = {63, 95, 127, 159, 191, 207, 175, 143, 111, 79, 47, 31, 
    63, 95, 127, 159, 191, 223, 239, 207, 175, 143, 111, 79, 47, 15, 0, 31, 63,
    95, 127, 159, 191, 223, 255, 239, 207, 175, 143, 111, 79, 47, 15, 31, 63, 95,
    127, 159, 191, 223, 207, 175, 143, 111, 79, 47, 63, 95, 127, 159, 191};
const char y_list[NUM_PIXELS] PROGMEM = {255, 255, 255, 255, 255, 223, 223, 223, 223, 223, 223,
    191, 191, 191, 191, 191, 191, 191, 159, 159, 159, 159, 159, 159, 159, 159, 127,
    127, 127, 127, 127, 127, 127, 127, 127, 95, 95, 95, 95, 95, 95, 95, 95, 63, 63,
    63, 63, 63, 63, 63, 31, 31, 31, 31, 31, 31, 0, 0, 0, 0, 0};

void add_circle(float x, float y, float r) {
    for(int i=0; i<NUM_PIXELS; i++) {
        float x_led = pgm_read_byte_near(x_list+i) / 255.0;
        float y_led = pgm_read_byte_near(y_list+i) / 255.0;
        float deltax = x_led - x;
        float deltay = y_led - y;
        float dist = sqrt(deltax*deltax +deltay*deltay);
        int value;
        float var = r / 10;
        if(dist < 47*var) {
            value = (int) 255 * exp(-0.5*dist*dist/var);
        } else {
            value = 0;
        }
        ledstrip.set_pixel_color(i, value, 0, 0);
    }
}

void setup() {
    clock_prescale_set(clock_div_1);    // set clock to 8 MHz
}

void loop(float time) {
    ledstrip.clear_pixels();
    add_circle(0.3*sin(time/1.84) + 0.5, 0.3*cos(time/2.3)+0.5, 0.04);
    ledstrip.show();
}

int main() {
    setup();
    float time = 0.0;
    while(1) {
        loop(time);
        time += 0.35;
    }
    return 0;
}
