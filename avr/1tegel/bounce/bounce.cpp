#define NUM_PIXELS 61

#include <avr/power.h>            // for setting clock
#include <avr/pgmspace.h>         //needed for PROGMEM
#include <math.h>
#include "leds.h"

float x=0.5;
float y=0.5;
const float ANGLE = 60.0 / 360.0 * 2 * 3.1415;
const float SPEED =0.03;
float vx, vy;

struct Point {
    float x, y;
};

Ledstrip ledstrip(255);

const char x_list[NUM_PIXELS] PROGMEM = {63, 95, 127, 159, 191, 207, 175, 143, 111, 79, 47, 31, 
    63, 95, 127, 159, 191, 223, 239, 207, 175, 143, 111, 79, 47, 15, 0, 31, 63,
    95, 127, 159, 191, 223, 255, 239, 207, 175, 143, 111, 79, 47, 15, 31, 63, 95,
    127, 159, 191, 223, 207, 175, 143, 111, 79, 47, 63, 95, 127, 159, 191};
const char y_list[NUM_PIXELS] PROGMEM = {255, 255, 255, 255, 255, 223, 223, 223, 223, 223, 223,
    191, 191, 191, 191, 191, 191, 191, 159, 159, 159, 159, 159, 159, 159, 159, 127,
    127, 127, 127, 127, 127, 127, 127, 127, 95, 95, 95, 95, 95, 95, 95, 95, 63, 63,
    63, 63, 63, 63, 63, 31, 31, 31, 31, 31, 31, 0, 0, 0, 0, 0};

void add_circle(float x, float y, float r) {
    for(int i=0; i<NUM_PIXELS; i++) {
        float x_led = pgm_read_byte_near(x_list+i) / 255.0;
        float y_led = pgm_read_byte_near(y_list+i) / 255.0;
        float deltax = x_led - x;
        float deltay = y_led - y;
        float dist = sqrt(deltax*deltax +deltay*deltay);
        int value;
        float var = r / 10;
        if(dist < 47*var) {
            value = (int) 255 * exp(-0.5*dist*dist/var);
        } else {
            value = 0;
        }
        ledstrip.set_pixel_color(i, value, 0, 0);
    }
}

int isLeftOf(Point p1, Point p2){
     return ((p2.x - p1.x)*(y - p1.y) - (p2.y - p1.y)*(x - p1.x)) > 0;
}

int isInHexagon() {
    Point right, topRight, topLeft, left, bottomLeft, bottomRight;
    right.x = 1;
    right.y = 0.5;
    topRight.x = 0.75;
    topRight.y = 1;
    topLeft.x = 0.25;
    topLeft.y = 1;
    left.x = 0;
    left.y = 0.5;
    bottomLeft.x = 0.25;
    bottomLeft.y = 0;
    bottomRight.x = 0.75;
    bottomRight.y = 0;

    return isLeftOf(right, topRight) && isLeftOf(topRight, topLeft) && isLeftOf(topLeft, left) && 
        isLeftOf(left, bottomLeft) && isLeftOf(bottomLeft, bottomRight) && isLeftOf(bottomRight, right);
}

// Reflect point p along line through points p0 and p1
Point reflect(Point p, Point p0, Point p1) {
    float nx = p1.x - p0.x;
    float ny = p1.y - p0.y;
    float d = sqrt(nx*nx + ny*ny);
    nx /= d;
    ny /= d;

    float px = p.x - p0.x;
    float py = p.y - p0.y;
    float w  = nx*px + ny*py;

    Point reflection;
    reflection.x = 2*p0.x - p.x + 2*w*nx;
    reflection.y = 2*p0.y - p.y + 2*w*ny;
    return reflection;
}

void bounceAll() {
    Point right, topRight, topLeft, left, bottomLeft, bottomRight, firstPoint, secondPoint;
    right.x = 1;
    right.y = 0.5;
    topRight.x = 0.75;
    topRight.y = 1;
    topLeft.x = 0.25;
    topLeft.y = 1;
    left.x = 0;
    left.y = 0.5;
    bottomLeft.x = 0.25;
    bottomLeft.y = 0;
    bottomRight.x = 0.75;
    bottomRight.y = 0;
    if(!isLeftOf(right, topRight)) {
        firstPoint = right;
        secondPoint = topRight;
    } else if(!isLeftOf(left, bottomLeft)) {
        firstPoint = left;
        secondPoint = bottomLeft;
    } else if(!isLeftOf(topRight, topLeft)) {
        firstPoint = topRight;
        secondPoint = topLeft;
    } else if(!isLeftOf(bottomLeft, bottomRight)) {
        firstPoint = bottomLeft;
        secondPoint = bottomRight;
    } else if(!isLeftOf(topLeft, left)) {
        firstPoint = topLeft;
        secondPoint = left;
    } else if(!isLeftOf(bottomRight, right)) {
        firstPoint = bottomRight;
        secondPoint = right;
    } 
    Point p0, p1;
    p0.x = 0.0f;
    p0.y = 0.0f;
    p1.x = firstPoint.x - secondPoint.x;
    p1.y = firstPoint.y - secondPoint.y;

    Point oldV, oldPoint;
    oldV.x = vx;
    oldV.y = vy;
    oldPoint.x = x;
    oldPoint.y = y;

    Point newV = reflect(oldV, p0, p1);
    Point newPoint = reflect(oldPoint, firstPoint, secondPoint);
    x = newPoint.x;
    y = newPoint.y;
    vx = newV.x;
    vy = newV.y;
}

void setup() {
    clock_prescale_set(clock_div_1);    // set clock to 8 MHz
    vx = SPEED * cos(ANGLE);
    vy = SPEED * sin(ANGLE);
}

void loop() {
    ledstrip.clear_pixels();
    x += vx;
    y += vy;
    if(!isInHexagon()) {
        bounceAll();
    }
    add_circle(x, y, 0.04);
    ledstrip.show();
}


int main() {
    setup();
    while(1) {
        loop();
    }
    return 0;
}
