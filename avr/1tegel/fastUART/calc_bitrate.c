/*
  Assumes that the sender sends a 1 every second, and other data otherwise.
  Calculates the bitrate in kbit/s and sends it back.
*/

#include <util/delay.h>
#include "USART.h"

void sendCounter() {
  uint16_t counter = 0;
  while (1) {
    printWord(counter++);
    printString("\n");
    _delay_ms(50);
  }
}

void echo() {
  while(1) {
    char serialCharacter = receiveByte();
    transmitByte(serialCharacter);
  }
}

void count() {
  uint32_t counter = 0;
  while(1) {
    char serialCharacter = receiveByte();
    counter++;
    if(serialCharacter == 1) {
      printWord(counter/125);
      printString(" kbit/s\n");
      counter = 0;
    }
  }
}

int main(void) {
  initUSART();
  count();
  return 0;
}
