#include <avr/io.h>
#include <avr/power.h>
#include <util/delay.h>
#include "softwareSerial.h"

#define DIFFERENTIAL 1
#define BIPOLAR 1
#define GAIN 1
#define VREF 5

#if BIPOLAR
  #define MAX_VALUE 512.
#else
  #define MAX_VALUE 1024.
#endif

#define FACTOR (VREF / (MAX_VALUE*GAIN)) 

SoftwareSerial serial;

static inline void initADC0(void) {
  // ADMUX |= (1 << REFS0);                            // reference voltage on AVCC
  ADMUX |= ((VREF == 1.1) << REFS1);                // Internal 1.1V reference 
  ADMUX |= (DIFFERENTIAL << MUX3);                  // Use ADC0 and ADC1 for differential voltage
  ADMUX |= ((DIFFERENTIAL && GAIN==20) << MUX0);    // Set gain
  ADCSRB |= (BIPOLAR << BIN);                       // bipolar mode
  ADCSRA |= (1 << ADPS2) | (1 << ADPS1);            // ADC clock prescaler /64
  ADCSRA |= (1 << ADEN);                            // enable ADC
}

void printFloat(float value) {
    if(value < 0) {
      serial.printString("-");
      value = -value;
    }
    uint8_t hundreds = uint16_t(value) / 100%10;
    uint8_t tens = uint16_t(value) / 10%10;
    uint8_t integers = uint16_t(value)%10;
    uint8_t tenths = uint16_t(value*10)%10;
    uint8_t hundredths = uint16_t(value*100)%10;
    uint8_t thousandths = uint16_t(value*1000)%10;
    if(hundreds)
      serial.printByte(hundreds);
    if(hundreds || tens)
      serial.printByte(tens);
    serial.printByte(integers);
    serial.printString(".");
    serial.printByte(tenths);
    serial.printByte(hundredths);
    serial.printByte(thousandths);
}

int main( void )
{
  clock_prescale_set(clock_div_1);    // set clock to 8 MHz
  serial.init();
  initADC0();

  while(1) {
    ADCSRA |= (1 << ADSC);                     // start ADC conversion
    loop_until_bit_is_clear(ADCSRA, ADSC);     // wait until done 
    int16_t adc_value = ADC;

    serial.printBinaryWord(adc_value);
    if(BIPOLAR) {
      adc_value <<= 6;  // Keep sign correct (10-bit to 16-bit upgrade)
      adc_value >>= 6;
    }

    float voltage = adc_value * FACTOR;

    serial.printString("        ");
    printFloat(voltage);
    serial.printString("V");
    serial.printString("        ");
    printFloat(voltage*500);
    serial.printString("mA");

    serial.printString("\n");
    _delay_ms(50);
  }
}

