#include <util/delay.h>
#include <avr/power.h>
#include <avr/io.h>
#include "USART.h"

#define START_MESSAGE 255

void setup() {
    // clock_prescale_set(clock_div_1);    // set clock to 8 MHz
    initUSART();
}

void loop() {
    set_USART_receive_mode();
    while(receiveByte() != START_MESSAGE) {}
    uint8_t source = receiveByte();
    uint8_t destination = receiveByte();
    uint8_t length = receiveByte();
    for(uint8_t i=0; i<length; i++) {
        receiveByte();
    }
    set_USART_transmit_mode();
    transmitByte(START_MESSAGE);
    transmitByte(destination);
    transmitByte(source);
    transmitByte(3);
    printByte(length);
    _delay_us(10);
}

int main() {
    setup();
    while(1) {
        loop();
    }
    return 0;
}
