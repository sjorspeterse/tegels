#define NUM_PIXELS 61
// Max brightness is 255
#define BRIGHTNESS 50

#include <util/delay.h>
#include <avr/power.h>
#include <avr/io.h>
#include <avr/pgmspace.h>         //needed for PROGMEM
#include "leds.h"
#include "USART.h"

Ledstrip ledstrip(BRIGHTNESS);

const uint8_t SIDES[6][5] PROGMEM = {
    {0, 1, 2, 3, 4},
    {4, 5, 17, 18, 34},
    {34, 35, 49, 50, 60},
    {60, 59, 58, 57, 56},
    {56, 55, 43, 42, 26},
    {26, 25, 11, 10, 0}
};


void setup() {
    clock_prescale_set(clock_div_1);    // set clock to 8 MHz
    initUSART();
    printString("Bring it on!\r\n");
}

void loop() {
    static int index = 0;

    for(int i=0; i<NUM_PIXELS; i++) {
        ledstrip.set_pixel_color(i, 0, 0, 0);
        for(int j=0; j<5; j++) {
            int addr = (int) SIDES + 5*index + j;
            int good_index = pgm_read_byte_near(addr);
            if(i == good_index) {
                ledstrip.set_pixel_color(i, 0, 0, 255);
            }
        }
    }

    ledstrip.show();
    char receivedByte = receiveByteNonBlocking();
    if(receivedByte == 'a') {
        index = (index+1) % 6;
        transmitByte(index + '0');
    }
    if(receivedByte == 'd') {
        index = (index+5) % 6;
        transmitByte(index + '0');
    }
}

int main() {
    setup();
    while(1) {
        loop();
    }
    return 0;
}
