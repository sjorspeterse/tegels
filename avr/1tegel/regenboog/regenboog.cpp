#define NUM_PIXELS 61
#define WAIT_TIME 1

#include <util/delay.h>
#include <avr/power.h>
#include "leds.h"

Ledstrip ledstrip(50);

static uint32_t Color(uint8_t r, uint8_t g, uint8_t b) {
  return ((uint32_t)r << 16) | ((uint32_t)g <<  8) | b;
}

void setPixelColor(int index, uint32_t color) {
  uint8_t r = color >> 16 & 0xff;
  uint8_t g = color >> 8 & 0xff;
  uint8_t b = color & 0xff;
  ledstrip.set_pixel_color(index, r, g, b);
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(uint8_t WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
   return Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else if(WheelPos < 170) {
    WheelPos -= 85;
   return Color(0, WheelPos * 3, 255 - WheelPos * 3);
  } else {
   WheelPos -= 170;
   return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  }
}

void rainbowCycle() {
    uint16_t i, j;
    for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
        for(i=0; i<NUM_PIXELS; i++) {
            setPixelColor(i, Wheel(((i * 256 / NUM_PIXELS + j) & 255)));
        }
        ledstrip.show();
        _delay_ms(WAIT_TIME);
    }
}

void setup() {
    // clock_prescale_set(clock_div_1);    // set clock to 8 MHz
}

void loop() {
    rainbowCycle();
}

int main() {
    setup();
    while(1) {
        loop();
    }
    return 0;
}
