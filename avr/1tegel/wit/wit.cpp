#define NUM_PIXELS 61

#include <avr/power.h>
#include "leds.h"

int main() {
    Ledstrip ledstrip(50);
    // clock_prescale_set(clock_div_1);    // set clock to 8 MHz
    while(1) {
        for(int i=0; i<NUM_PIXELS; i++) {
            ledstrip.set_pixel_color(i, 255, 255, 255);
        }
        ledstrip.show();
    }
    return 0;
}
