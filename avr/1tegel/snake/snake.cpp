#include <util/delay.h>
#include <avr/power.h>
#include "leds.h"

#define NUM_PIXELS 61

Ledstrip ledstrip(255);

int main()
{
  int r = 0;
  int g = 0;
  int b = 128;
  // clock_prescale_set(clock_div_1);    // set clock to 8 MHz
  while (1) {
    for(int i=0;i<NUM_PIXELS;i++) {
      ledstrip.set_pixel_color(i, r, g, b);
      ledstrip.set_pixel_color((i-5 + NUM_PIXELS)%NUM_PIXELS, 0, 0, 0);
      ledstrip.show();
      _delay_ms(100);
    }
  }
}
