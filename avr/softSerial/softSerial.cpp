#include "softwareSerial.h"
#include <avr/power.h>            // for setting clock

SoftwareSerial serial;

int main( void )
{
  clock_prescale_set(clock_div_1);    // set clock to 8 MHz
  serial.init();
  serial.printString( "Hallo Peter\n\r" );

  while(1) {
    while(serial.dataAvailable()) {
      uint8_t char_in = serial.receiveByte() + 1;
      serial.transmitByte(char_in);
    }
  }
}
