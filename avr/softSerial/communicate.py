SERIAL_PORT = "/dev/ttyUSB1"
BAUD_RATE = 9600
# BAUD_RATE = 38400

import serial
from time import sleep

sp = serial.Serial(SERIAL_PORT, BAUD_RATE, timeout = 1)
sp.flush()
print ('Serial name:', sp.name)

while(1):
    response = sp.read(1)  # Read one byte
    if response:
        response_int = int(response[0])
        print(response_int, flush=True)
        sp.write([response_int])
    else:
        # sp.write(b'hello\n')
        print('Restarting!')
        sp.write([0])

        sleep(1)
