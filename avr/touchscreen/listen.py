SERIAL_PORT = "/dev/ttyUSB0"
BAUD_RATE = 9600
# BAUD_RATE = 38400

import serial
from time import sleep

sp = serial.Serial(SERIAL_PORT, BAUD_RATE, timeout = 1)
sp.flush()
print ('Serial name:', sp.name)

while(1):
    response = sp.readline()
    if response:
        print(response, flush=True)
