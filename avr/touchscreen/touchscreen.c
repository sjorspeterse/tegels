/*
   Capacitive touch sensor demo. In this test we use NO/PA3/PCINT3 as touch pin
*/

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/power.h>            // for setting clock
#include "softwareSerial.h"
#include "leds.h"

#define SENSE_TIME   50
#define THRESHOLD    3085

#define CAP_SENSOR_PORT   PORTA
#define CAP_SENSOR_DDR    DDRA
#define CAP_SENSOR        PA3

// -------  Global Variables ---------- //
volatile uint16_t chargeCycleCount;

SoftwareSerial serial;
Ledstrip ledstrip(50);

void initPinChangeInterrupt(void) {
  GIMSK |= (1 << PCIE0);    /* enable Pin-change interrupts 0 (bank A) */  
  PCMSK0 |= (1 << PCINT3);   /* enable specific interrupt for our pin PA3 */
  sei();
}

ISR(PCINT0_vect) {
  chargeCycleCount++;                             /* count this change */

  CAP_SENSOR_DDR |= (1 << CAP_SENSOR);                  /* output mode */
  _delay_us(1);                                      /* charging delay */

  CAP_SENSOR_DDR &= ~(1 << CAP_SENSOR);                /* set as input */
  GIFR |= (1 << PCIF0);             /* clear the pin-change interrupt */
}

int main(void)
{
  clock_prescale_set(clock_div_1);    // set clock to 8 MHz
  serial.init();
  initPinChangeInterrupt();
  MCUCR |= (1 << PUD);                          /* disable all pullups */
  CAP_SENSOR_PORT |= (1 << CAP_SENSOR);    /* we can leave output high */
  CAP_SENSOR_DDR |= (1 << CAP_SENSOR);       /* start with cap charged  Maybe this in loop?*/

  while(1) {
    chargeCycleCount = 0;                             /* reset counter */
    _delay_ms(SENSE_TIME);
    uint16_t touchValue = chargeCycleCount;

    if(touchValue < 7750) {
      ledstrip.set_pixel_color(0, 0, 255, 0);
    } else {
      ledstrip.set_pixel_color(0, 0, 0, 0);
    }
    
    ledstrip.show(); // danger, if capacitor has a falling edge during ledstrip show, interrupt might be missed?
      // Anyway, giving it a rising edge fixes the problem
    serial.printWord(touchValue);
    serial.printString("\n");
  }
}