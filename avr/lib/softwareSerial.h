#ifndef _suart_h_
#define _suart_h_

#include <avr/pgmspace.h>
#include <avr/io.h>
#include <avr/interrupt.h>
//			Easier type writing:

typedef   signed char  s8;
typedef unsigned short u16;
typedef   signed short s16;
typedef unsigned long  u32;
typedef   signed long  s32;


// 			Access bits like variables:

struct bits {
  uint8_t b0:1;
  uint8_t b1:1;
  uint8_t b2:1;
  uint8_t b3:1;
  uint8_t b4:1;
  uint8_t b5:1;
  uint8_t b6:1;
  uint8_t b7:1;
} __attribute__((__packed__));

#define SBIT_(port,pin) ((*(volatile struct bits*)&port).b##pin)
#define	SBIT(x,y)	SBIT_(x,y)


//			Optimization improvements


#define	OPTR18 __asm__ volatile (""::);		// it helps, but why ?
						// remove useless R18/R19

// always inline function x:

#define AIL(x)   static x __attribute__ ((always_inline)); static x


// never inline function x:

#define NIL(x)   x __attribute__ ((noinline)); x


// volatile access (reject unwanted removing access):

#define vu8(x)  (*(volatile uint8_t*)&(x))
#define vs8(x)  (*(volatile s8*)&(x))
#define vu16(x) (*(volatile u16*)&(x))
#define vs16(x) (*(volatile s16*)&(x))
#define vu32(x) (*(volatile u32*)&(x))
#define vs32(x) (*(volatile s32*)&(x))

#include <util/delay.h>

// Tx: MOSI/SDA, PA6
// Rx: noord, PA7
#define	STXD 		SBIT( PORTA, PA6 )	// = OC1A
#define	STXD_DDR	SBIT( DDRA,  PA6 )
#define	SRXD_PIN	SBIT( PINA,  PA7 )	// = ICP

			// size must be in range 2 .. 256
#define STX_SIZE	20
#define	SRX_SIZE	20

class SoftwareSerial {
  public:
	  void init();
	  void transmitByte(uint8_t byte);
    void printByte(uint8_t byte);
    void printBinaryByte(uint8_t byte);
    void printWord(uint16_t word);
    void printSignedWord(int16_t word);
    void printBinaryWord(uint16_t word);
	  void printString(const char myString[]);
	  bool dataAvailable();
	  uint8_t receiveByte();
  protected:
  private:
};

#endif
