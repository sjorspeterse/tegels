// This is AVR code for driving the RGB LED strips from Pololu.
//
// This version can control any AVR pin.  It uses "lds" and "sts" instructions,
// which means it has to write to the entire port register every time it toggles
// the I/O line, but that should be safe because interrupts are disabled.
//
// It allows complete control over the color of an arbitrary number of LEDs.
// This implementation disables interrupts while it does bit-banging with
// inline assembly.

// This code supports 20 MHz, 16 MHz and 8MHz

#ifndef LEDS_H
#define LEDS_H

#define LED_STRIP_PORT PORTB
#define LED_STRIP_DDR  DDRB
#define LED_STRIP_PIN  1

// The rgb_color struct represents the color for an 8-bit RGB LED.
// Examples:
//   Black:      (rgb_color){ 0, 0, 0 }
//   Pure red:   (rgb_color){ 255, 0, 0 }
//   Pure green: (rgb_color){ 0, 255, 0 }
//   Pure blue:  (rgb_color){ 0, 0, 255 }
//   White:      (rgb_color){ 255, 255, 255}
struct rgb_color
{
  uint8_t red, green, blue;
};

class Ledstrip {
  public:
    Ledstrip(uint8_t brightness);
    void set_pixel_color(int index, uint8_t r, uint8_t g, uint8_t b);
    void clear_pixels();
    void __attribute__((noinline)) show();
    static const int num_leds=61;
  protected:
  private:
    rgb_color m_buffer[num_leds];
    uint8_t m_brightness;
};

#endif