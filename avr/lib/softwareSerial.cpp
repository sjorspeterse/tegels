/************************************************************************/
/*                                                                      */
/*                      Software UART with FIFO, using T1               */
/*                                                                      */
/*              Author: P. Dannegger                                    */
/*                                                                      */
/*   Source:                                                            */
/*   https://www.avrfreaks.net/projects/software-uart-fifo?item_id=1895 */
/*                                                                      */
/*    Added by Sjors:                                                   */
/*    Connect the orange serial TX cable to N (middle pin on top)       */
/*    Connect the yellow serial RX cable to any SDA                     */
/*    Connect the black serial GND to any GND                           */
/*                                                                      */
/*		Target: ATtiny44 (Tested on ATtiny84A)                            */
/*                                                                      */
/************************************************************************/
#include "softwareSerial.h"

#define BIT_TIME	(u16)(F_CPU * 1.0 / BAUD + 0.5)

#define	TX_HIGH		(1<<COM1A1^1<<COM1A0)
#define	TX_LOW		(TX_HIGH^1<<COM1A0)
#define	TX_OUT		TCCR1A		// use compare output mode

#define ROLLOVER( x, max )	x = ++x >= max ? 0 : x
					// count up and wrap around

static uint8_t stx_buff[STX_SIZE];
static uint8_t stx_in;
static uint8_t stx_out;
static uint8_t stx_data;
static uint8_t stx_state;

static uint8_t srx_buff[SRX_SIZE];
static uint8_t srx_in;
static uint8_t srx_out;
static uint8_t srx_data;
static uint8_t srx_state;

void SoftwareSerial::init() {
  sei();   // allow interupts
  OCR1A = BIT_TIME - 1;
  TCCR1A = TX_HIGH;			// set OC1A high, T1 mode 4
  TCCR1B = 1<<ICNC1^1<<WGM12^1<<CS10;	// noise canceler, 1-0 transition,
					// CLK/1, T1 mode 4 (CTC)
  TCCR1C = 1<<FOC1A;
  stx_state = 0;
  stx_in = 0;
  stx_out = 0;
  srx_in = 0;
  srx_out = 0;
  STXD_DDR = 1;				// output enable
  TIFR1 = 1<<ICF1;			// clear pending interrupt
  TIMSK1 = 1<<ICIE1^1<<OCIE1A;		// enable tx and wait for start
}

bool SoftwareSerial::dataAvailable() {			// check, if rx buffer not empty
  return (srx_out ^ vu8(srx_in)) > 0;
}


uint8_t SoftwareSerial::receiveByte()	{		// wait until byte received
  uint8_t data;

  while(!dataAvailable());		     	// until at least one byte in
  data = srx_buff[srx_out];		// get byte
  ROLLOVER(srx_out, SRX_SIZE);
  return data;
}


void SoftwareSerial::transmitByte(uint8_t byte) {		// transmit byte
  uint8_t i = stx_in;

  ROLLOVER(i, STX_SIZE);
  stx_buff[stx_in] = ~byte;		// complement for stop bit after data
  while( i == vu8(stx_out));		// until at least one byte free
					// stx_out modified by interrupt !
  stx_in = i;
}

// Converts a byte to a string of decimal text, sends it
void SoftwareSerial::printByte(uint8_t byte) {
  uint8_t hundreds = byte / 100;
  uint8_t tens = (byte / 10) % 10;
  if(hundreds)
    transmitByte('0' + hundreds);
  if(hundreds || tens)
    transmitByte('0' + tens);
  transmitByte('0' + (byte % 10));
}

void SoftwareSerial::printWord(uint16_t word) {
  uint8_t tenhousands = word / 10000;
  uint8_t thousands = (word / 1000) % 10;
  uint8_t hundreds = (word / 100) % 10;
  uint8_t tens = (word / 10) % 10;
  if(tenhousands)
    transmitByte('0' + tenhousands);
  if(tenhousands || thousands)
    transmitByte('0' + thousands);
  if(tenhousands || thousands || hundreds)
    transmitByte('0' + hundreds);
  if(tenhousands || thousands || hundreds || tens)
    transmitByte('0' + tens);
  transmitByte('0' + (word % 10));
}

void SoftwareSerial::printSignedWord(int16_t word) {
  uint16_t magnitude = word;
  if(word < 0) {
    printString("-");
    magnitude = ~word+1;
  }
  printWord(magnitude);
}

void SoftwareSerial::printBinaryByte(uint8_t byte) {
                       /* Prints out a byte as a series of 1's and 0's */
  uint8_t bit;
  for (bit = 7; bit < 255; bit--) {
    if (bit_is_set(byte, bit))
      transmitByte('1');
    else
      transmitByte('0');
  }
}

void SoftwareSerial::printBinaryWord(uint16_t word) {
                       /* Prints out a byte as a series of 1's and 0's */
  uint8_t bit;
  for (bit = 15; bit < 255; bit--) {
    if (word & (1 << bit))
      transmitByte('1');
    else
      transmitByte('0');
  }
}

void SoftwareSerial::printString(const char myString[]) {
  for(uint8_t i=0; myString[i]; i++) {
    transmitByte(myString[i]);
  }
}

/******************************	Interrupts *******************************/

ISR(TIM1_CAPT_vect)	{			// start detection
  s16 i = ICR1 - BIT_TIME / 2;			// scan at 0.5 bit time

  OPTR18					// avoid disoptimization
  if(i < 0)
    i += BIT_TIME;				// wrap around
  OCR1B = i;
  srx_state = 10;
  TIFR1 = 1<<OCF1B;				// clear pending interrupt
  if(SRXD_PIN == 0)				// still low
    TIMSK1 = 1<<OCIE1A^1<<OCIE1B;		// wait for first bit
}


ISR(TIM1_COMPB_vect)				// receive data bits
{
  uint8_t i;

  switch(--srx_state) {

    case 9:
      if(SRXD_PIN == 0)		// start bit valid
        return;
	    break;

    default: 
      i = srx_data >> 1;	      		// LSB first
	    if(SRXD_PIN == 1)
	      i |= 0x80;			// data bit = 1
	    srx_data = i;
	    return;

    case 0:  
      if(SRXD_PIN == 1) {		// stop bit valid
	      i = srx_in;
	      ROLLOVER(i, SRX_SIZE);
	      if(i != srx_out) {		// no buffer overflow
          srx_buff[srx_in] = srx_data;
		      srx_in = i;			// advance in pointer
	      }
	    }
	    TIFR1 = 1<<ICF1;			// clear pending interrupt
  }
  TIMSK1 = 1<<ICIE1^1<<OCIE1A;			// enable next start
}


ISR(TIM1_COMPA_vect)				// transmit data bits
{
  if(stx_state) {
    stx_state--;
    TX_OUT = TX_HIGH;
    if(stx_data & 1)				// lsb first
      TX_OUT = TX_LOW;
    stx_data >>= 1;
    return;
  }
  if(stx_in != stx_out) {			// next byte to sent
    stx_data = stx_buff[stx_out];
    ROLLOVER(stx_out, STX_SIZE);
    stx_state = 9;				// 8 data bits + stop
    TX_OUT = TX_LOW;				// start bit
  }
}
