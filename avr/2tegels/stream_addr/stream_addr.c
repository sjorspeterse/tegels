#include <util/delay.h>
#include <avr/power.h>
#include <avr/io.h>
#include "USART.h"
#include "leds.h"

#define TILE_INDEX 1
#define START_MESSAGE 255

#define NUM_PIXELS 61
// Max brightness is 255
#define BRIGHTNESS 50
Ledstrip ledstrip(BRIGHTNESS);

void setup() {
    // clock_prescale_set(clock_div_1);    // set clock to 8 MHz
    initUSART();
}

uint8_t DATA[61*3];

uint8_t receive_data() {
    set_USART_receive_mode();
    while(receiveByte() != START_MESSAGE) {}
    uint8_t source = receiveByte();
    uint8_t destination = receiveByte();
    uint8_t length = receiveByte();
    for(uint8_t i=0; i<length; i++) {
        DATA[i] = receiveByte();
    }
    return destination;
}

void show_leds() {
    for(int i=0; i<NUM_PIXELS; i++) {
        ledstrip.set_pixel_color(i, DATA[i*3], DATA[i*3+1], DATA[i*3+2]);
    }

    ledstrip.show();
}

void loop() {
    uint8_t tile_index = receive_data();
    if (tile_index == TILE_INDEX) {
        show_leds();
    }
}

int main() {
    setup();
    while(1) {
        loop();
    }
    return 0;
}
