#include <avr/io.h>
#include <util/delay.h>

#define LED             PB0
#define LED_DDR         DDRB
#define LED_PORT        PORTB

#define DOT_TIME        20
#define DASH_TIME       (3 * DOT_TIME)
#define BIT_SPACE       DOT_TIME
#define LETTER_SPACE    (2 * DOT_TIME)
#define WORD_SPACE      (6 * DOT_TIME)

#define setBit(sfr, bit)     (_SFR_BYTE(sfr) |= (1 << bit))
#define clearBit(sfr, bit)   (_SFR_BYTE(sfr) &= ~(1 << bit))

void dot() {
    setBit(LED_PORT, LED);
    _delay_ms(DOT_TIME);
    clearBit(LED_PORT,LED);
    _delay_ms(BIT_SPACE);
}

void dash() {
    setBit(LED_PORT, LED);
    _delay_ms(DASH_TIME);
    clearBit(LED_PORT, LED);
    _delay_ms(BIT_SPACE);
}

void H() {
    dot();
    dot();
    dot();
    dot();
    _delay_ms(LETTER_SPACE);
}

void M() {
    dash();
    dash();
    _delay_ms(LETTER_SPACE);
}

void O() {
    dash();
    dash();
    dash();
    _delay_ms(LETTER_SPACE);
}

void S() {
    dot();
    dot();
    dot();
    _delay_ms(LETTER_SPACE);
}

void setup() {
    setBit(LED_DDR, LED);                      /* set LED pin for output */
}

void loop() {
    S();
    O();
    S();
    _delay_ms(WORD_SPACE);
}

int main() {
    setup();
    while(1) {
        loop();
    }
    return 0;
}