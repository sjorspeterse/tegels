int DOT_TIME = 200;
int DASH_TIME = 3 * DOT_TIME;
int BIT_SPACE = DOT_TIME;
int LETTER_SPACE = 2 * DOT_TIME;
int WORD_SPACE = 6 * DOT_TIME;

int OUT_PIN = 0;

void setup() {
   pinMode(OUT_PIN, OUTPUT);
}

void loop() {
  S();
  O();
  S();
  delay(WORD_SPACE);
}

void dot() {
  digitalWrite(OUT_PIN, HIGH);
  delay(DOT_TIME);
  digitalWrite(OUT_PIN, LOW);
  delay(BIT_SPACE);
}

void dash() {
  digitalWrite(OUT_PIN, HIGH);
  delay(DASH_TIME);
  digitalWrite(OUT_PIN, LOW);
  delay(BIT_SPACE);
}

void H() {
  dot();
  dot();
  dot();
  dot();
  delay(LETTER_SPACE);
}

void M() {
  dash();
  dash();
  delay(LETTER_SPACE);
}

void O() {
  dash();
  dash();
  dash();
  delay(LETTER_SPACE);
}

void S() {
  dot();
  dot();
  dot();
  delay(LETTER_SPACE);
}
