/*
 * ATTiny85 Demo Running to 61-LED NeoPixel Ring 
 * https://www.pcboard.ca/neopixel-ring
 *  
 * Original Code: Stephan Martin 2016
 * 
 * Brightness is reduced as there is only USB power supply.
 * 
 */


#include <Adafruit_NeoPixel.h>    //needed for the WS2812
#include <avr/pgmspace.h>         //needed for PROGMEM
#include "FRAME_DATA.h"

#define PIN 9                     
#define BRIGHTNESS 40             // brightness reduced
#define NUM_LEDS 61
// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRB + NEO_KHZ800);



//const LED_HEX PROGMEM

void setup() {
  pinMode(PIN, OUTPUT);
  strip.begin();
  strip.setBrightness(BRIGHTNESS); // set brightness
  strip.show(); // Initialize all pixels to 'off'
}

void loop() {
  showFrame();
  delay(10);
}

void showFrame() {
  for(int i=0; i< strip.numPixels(); i++) {
    strip.setPixelColor(i, Pixel(i));
  }
  strip.show();
}

uint32_t Pixel(byte index) {
   uint8_t red, green, blue;
   int color_addr = LED_DATA + index*3;
   red = pgm_read_byte_near(LED_DATA+index*3+0);
   green = pgm_read_byte_near(LED_DATA+index*3+1);
   blue = pgm_read_byte_near(LED_DATA+index*3+2);
   return strip.Color(green, red, blue);
}
