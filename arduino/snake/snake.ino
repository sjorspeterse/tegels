#include <Adafruit_NeoPixel.h>

#define PIN            9
#define NUMPIXELS      61
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
int delayval = 100;

void setup() {
  strip.begin();
}

void loop() {
  int r = 0;
  int g = 0;
  int b = 128;
  for(int i=0;i<NUMPIXELS;i++){
    strip.setPixelColor(i, strip.Color(g, r, b));
    strip.setPixelColor((i-5 + NUMPIXELS)%NUMPIXELS, strip.Color(0, 0, 0));
    strip.show(); // This sends the updated pixel colors to the hardware.
    delay(delayval);
  }
}
