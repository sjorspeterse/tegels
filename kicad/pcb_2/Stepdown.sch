EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L StepDown:StepDown PS1
U 1 1 624508B0
P 2150 1350
F 0 "PS1" H 2150 1717 50  0000 C CNN
F 1 "StepDown" H 2150 1626 50  0000 C CNN
F 2 "StepDown" H 2000 1100 50  0001 L CNN
F 3 "https://power.murata.com/pub/data/power/ncl/kdc_mee1.pdf" H 3200 1050 50  0001 L CNN
	1    2150 1350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62452A52
P 1750 1450
F 0 "#PWR?" H 1750 1200 50  0001 C CNN
F 1 "GND" H 1755 1277 50  0000 C CNN
F 2 "" H 1750 1450 50  0001 C CNN
F 3 "" H 1750 1450 50  0001 C CNN
	1    1750 1450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62452E00
P 2550 1450
F 0 "#PWR?" H 2550 1200 50  0001 C CNN
F 1 "GND" H 2555 1277 50  0000 C CNN
F 2 "" H 2550 1450 50  0001 C CNN
F 3 "" H 2550 1450 50  0001 C CNN
	1    2550 1450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
